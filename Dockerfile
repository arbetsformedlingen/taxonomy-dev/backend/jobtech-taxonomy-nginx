FROM nginx:1.22
COPY nginx.conf /etc/nginx/nginx.conf

RUN chgrp -R 0 /etc/nginx  && chmod -R g=u /etc/nginx
RUN chgrp -R 0 /var/cache/nginx  && chmod -R g=u /var/cache/nginx
RUN chgrp -R 0 /var/log/nginx  && chmod -R g=u /var/log/nginx
RUN chgrp -R 0 /var/run/ && chmod -R g=u /var/run/
